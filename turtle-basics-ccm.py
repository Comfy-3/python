import turtle
# main function
def main():
	t = turtle.Turtle()
	t.ht()
	screen = turtle.Screen()

	turtle.tracer(0, 0)
	t.width(4)
	t.pencolor("#000000")
	t.fillcolor("#000000")
	t.penup()
	t.goto(0,50)
	t.pendown()
	t.goto(0,-45)
	t.penup()
	t.goto(-10,-45)
	t.pendown()
	t.goto(-15,-30)
	t.goto(0,0)
	t.penup()
	t.goto(10,-45)
	t.pendown()
	t.goto(15,-30)
	t.goto(0,0)
	t.penup()
	t.goto(0,-65)
	t.pendown()
	t.begin_fill()
	t.circle(8)
	t.end_fill()

	
	screen.exitonclick()	
	

if __name__ == "__main__":
	main()
